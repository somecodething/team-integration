package org.MC3.sg;

import java.util.ArrayList;
import java.util.HashMap;

import org.MC3.sg.world.Maps;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Vars {
	public static String prefix = "§3[SG]: §a";
	
	public static Maps map = null;
	
	public static String nextEvent = "Chest Refill";
	
	public static boolean ingame = false;
	public static boolean starting = false;
	public static boolean canStart = true;

	public static long startTime = 0;
	
	public static ArrayList<Player> participants = new ArrayList<Player>();
	public static ArrayList<Player> GodMode = new ArrayList<Player>();
	public static ArrayList<Location> openedChests = new ArrayList<Location>();
	
	public static HashMap<Player, Player> lastDamager = new HashMap<Player, Player>();
	public static HashMap<Player, Integer> kills = new HashMap<Player, Integer>();
	
	public static int deaths = 0;
	public static int requiredPlayerCount = 2;
	
	public static double tier2Chance = 0.2;

    // Teams
	public static ArrayList<Integer> teamID = new ArrayList<Integer>();
	public static HashMap<ArrayList<Integer>, Player> teamLeader = new HashMap<ArrayList<Integer>, Player>();
	public static HashMap<Player, ArrayList<Integer>> team = new HashMap<Player, ArrayList<Integer>>();
	public static HashMap<Player, String> inviting = new HashMap<Player, String>();
	public static HashMap<Player, Boolean> invited = new HashMap<Player, Boolean>();
}