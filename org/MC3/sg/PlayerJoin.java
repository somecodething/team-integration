package org.MC3.sg;

import org.MC3.sg.game.Scoreboard;
import org.MC3.sg.game.Spec;
import org.MC3.sg.world.Maps;
import org.MC3.sg.world.MidLocs;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import me.Swedz.api.API;

public class PlayerJoin implements Listener {
	public static void GoToHub(Player player) {
		player.sendMessage("§aConnecting you to Hub...");
		API.getBungeeAPI().connect(player, "Hub");
	}
	
	@EventHandler
	public void onHungerChange(FoodLevelChangeEvent e) {
		if(Vars.ingame == false) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		if(Vars.ingame == false) {
			Spec.set(e.getPlayer(), false);
		} else {
			Location loc = MidLocs.locs.get(Maps.getCurrent());
			loc.setYaw(-180); loc.setPitch(0);
			e.setRespawnLocation(loc);
			Spec.set(e.getPlayer(), true);
		}
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) throws Exception {
		Player player = e.getPlayer();
		Freeze.set(player, false);
		
		player.spigot().respawn();
		player.setLevel(0);
		player.setExp(0);
		
		if(Vars.ingame == false) {
			Spec.set(e.getPlayer(), false);
			
			player.setHealth(20.0);
			player.setFoodLevel(20);
			
			PlayerInventory inv = player.getInventory();
			ItemStack air = new ItemStack(Material.AIR);
			
			player.setFoodLevel(20);
			
			Location loc = new Location(Bukkit.getWorld("world"), 0.5, 65, 0.5);
			loc.setYaw(-180); loc.setPitch(0);
			player.teleport(loc);
			
			player.setGameMode(GameMode.ADVENTURE);
			e.setJoinMessage(API.getChatAPI().prefix("bonus", player.getName()) + API.getChatAPI().prefix("main", player.getName()) + API.getChatAPI().namecolor(player.getName()) + player.getName() + " §6joined the game (" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers() + ")");
			
			player.sendMessage(ChatColor.RED + "=============================");
			player.sendMessage(ChatColor.DARK_GREEN + "Welcome to MC³ Survival Games!");
			player.sendMessage(ChatColor.RED + "=============================");
			
			inv.clear();
			inv.setHelmet(air);
			inv.setChestplate(air);
			inv.setLeggings(air);
			inv.setBoots(air);
			
			ItemStack hub = API.getItemAPI().createItem(new ItemStack(Material.BED), ChatColor.GREEN + "Leave Game", new String[] {}, null, false, true);
			inv.setHeldItemSlot(8);
			inv.setItemInHand(hub);
			inv.setHeldItemSlot(0);	
		} else {
			Spec.set(player, true);
			player.teleport(new Location(Bukkit.getWorld("Game"), 0.5, MidLocs.maxHeight.get(Maps.getCurrent()), 0.5));
			player.setFlying(true);
			
			player.setHealth(20.0);
			player.setFoodLevel(20);
			
			e.setJoinMessage(null);
		}
		
		Scoreboard.set(player);
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) throws Exception {
		Player player = e.getPlayer();
		
		if(Vars.ingame == false) {	
			e.setQuitMessage(API.getChatAPI().colorize(API.getChatAPI().prefix("bonus", player.getName())) + API.getChatAPI().prefix("main", player.getName()) + API.getChatAPI().namecolor(player.getName()) + player.getName() + " §6left the game (" + (Bukkit.getOnlinePlayers().size()-1) + "/" + Bukkit.getMaxPlayers() + ")");
		} else {
			e.setQuitMessage(null);
		}
		
		Main.instance.getServer().getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {
			public void run() {
				for(Player lp : Bukkit.getOnlinePlayers()) {
					Scoreboard.set(lp);
				}
			}
		}, 20);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		Action a = e.getAction();
		ItemStack is = e.getItem();
		
		if (a == Action.PHYSICAL || is == null || is.getType() == Material.AIR)
			return;
		
		if (is.getType() == Material.BED && Vars.ingame == false) {
			if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK)
				GoToHub(e.getPlayer());
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (e.getCause() == DamageCause.VOID && e.getEntity() instanceof Player) {
			if (Vars.ingame == false) {
				Location loc = new Location(Bukkit.getWorld("world"), 0.5, 65, 0.5);
				loc.setYaw(-180); loc.setPitch(0);
				e.getEntity().teleport(loc);
				e.setCancelled(true);
			}
		}
	}
}