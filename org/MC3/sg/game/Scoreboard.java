package org.MC3.sg.game;

import org.MC3.sg.Main;
import org.MC3.sg.Vars;
import org.MC3.sg.world.Maps;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.ScoreboardManager;

public class Scoreboard {
	public static void set(Player player) {
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		org.bukkit.scoreboard.Scoreboard board = manager.getNewScoreboard();
		
		new BukkitRunnable() {
			@Override
			public void run() {
				Objective sb = board.registerNewObjective(player.getName(), "dummy");
				sb.setDisplayName("§e§lMCCUBED");
				sb.setDisplaySlot(DisplaySlot.SIDEBAR);
				
				if(Vars.ingame == false && Vars.starting == false) {
					sb.getScore("Server: §b#" + Bukkit.getServerName().replace("SG-", "")).setScore(8);
					sb.getScore("§1").setScore(7);
					sb.getScore("Players: §b" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers()).setScore(6);
					sb.getScore("§7Waiting for players..").setScore(5);
					sb.getScore("§2").setScore(4);
					sb.getScore("Map: §b" + Maps.getCurrent().toString()).setScore(3);
					sb.getScore("§3").setScore(2);
					sb.getScore("§amc-cubed.org").setScore(1);
				} else if(Vars.ingame == false && Vars.starting == true) {
					sb.getScore("Server: §b#" + Bukkit.getServerName().replace("SG-", "")).setScore(8);
					sb.getScore("§1").setScore(7);
					sb.getScore("Players: §b" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers()).setScore(6);
					sb.getScore("§7Game is starting soon..").setScore(5);
					sb.getScore("§2").setScore(4);
					sb.getScore("Map: §b" + Maps.getCurrent().toString()).setScore(3);
					sb.getScore("§3").setScore(2);
					sb.getScore("§amc-cubed.org").setScore(1);
				} else if(Vars.ingame == true) {
					sb.getScore("Server: §b#" + Bukkit.getServerName().replace("SG-", "")).setScore(12);
					sb.getScore("§1").setScore(11);
					sb.getScore("Next Event:").setScore(10);
					sb.getScore("§b" + Vars.nextEvent).setScore(9);
					sb.getScore("§2").setScore(8);
					sb.getScore("Players: §b" + Vars.participants.size() + "/" + Bukkit.getMaxPlayers()).setScore(7);
					sb.getScore("Deaths: §b" + Vars.deaths).setScore(6);
					sb.getScore("Kills: §b" + Vars.kills.get(player)).setScore(5);
					sb.getScore("§3").setScore(4);
					sb.getScore("Map: §b" + Maps.getCurrent().toString()).setScore(3);
					sb.getScore("§4").setScore(2);
					sb.getScore("§amc-cubed.org").setScore(1);
				}
				
				player.setScoreboard(board);
			}
		}.runTaskAsynchronously(Main.instance);
	}
}