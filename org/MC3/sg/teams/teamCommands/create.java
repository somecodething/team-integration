package org.MC3.sg.teams.teamCommands;

import org.MC3.sg.Vars;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class create implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		if (commandLabel.equalsIgnoreCase("team")) {
			if (args.length == 1 && args[0].equalsIgnoreCase("create") && sender instanceof Player) {
				Player s = (Player) sender;
			
				sender.sendMessage("You have created a team.");
				Vars.teamLeader.put(Vars.teamID, s);
				Vars.teamID.add(+1);
				Vars.teamID.equals(Vars.teamLeader);
			}
		
			if (Vars.teamID.contains("\\d+")) {
				sender.sendMessage(ChatColor.RED + "You must leave your current team to create a new one.");
			}
		}
		return true;
	}
}