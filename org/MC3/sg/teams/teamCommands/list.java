package org.MC3.sg.teams.teamCommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class list implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player target;

		if (args.length == 0 && !(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
			return true;

		} else if (args.length == 0) {
			target = (Player) sender;
		} else {
			target = Bukkit.getServer().getPlayer(args[0]);

			if (target == null) {
				sender.sendMessage(ChatColor.RED + "Cannot find player '" + args[0] + "'");
				return true;
			}
		}
		if (commandLabel.equalsIgnoreCase("team")) {
			if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
				if (sender == target) {
					sender.sendMessage("Players: This is where the list of players in your team will be");
					return true;
				} else {
					sender.sendMessage("Players: This is where the list of players in a team will be");	
					return true;
				}
			sender.sendMessage("Players: This is where the list of players in your team will be");
			}
		}
		return true;
	}

}
