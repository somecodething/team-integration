package org.MC3.sg.teams.teamCommands;

import org.MC3.sg.Vars;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class invite implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player target;
		target = Bukkit.getServer().getPlayer(args[0]);
		Player s = (Player) sender;
		
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
			return true;
		}
		if (commandLabel.equalsIgnoreCase("team")) {
			if (Vars.teamLeader.get(Vars.team.get(s)) != null) {
				if (args.length == 1 && args[0].equalsIgnoreCase("invite") && s != target) {

					if (target == null)
						s.sendMessage("Cannot find player '" + args[0] + "'");
	
					s.sendMessage(ChatColor.GREEN + "You have invited " + target.getName() + " to your team.");
					target.sendMessage(ChatColor.GREEN + "You have been invited to " + s.getName() + "'s team.");
				
					Vars.inviting.put(s, "\\D+");
					Vars.invited.put(target, Vars.invited.equals(Vars.inviting));

				} else {
					s.sendMessage(ChatColor.DARK_RED + "You cannot invite your self to your own team!");
				}
			} else {
				s.sendMessage(ChatColor.DARK_RED + "You do not have permission to invite players. Ask your team leader!");
			}
		}
		return true;
	}
}