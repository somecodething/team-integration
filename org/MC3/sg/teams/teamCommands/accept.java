package org.MC3.sg.teams.teamCommands;

import org.MC3.sg.Vars;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class accept implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player target;

		if (args.length == 0 && !(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
			return true;

		} else if (args.length == 0) {
			target = (Player) sender;
			sender.sendMessage(ChatColor.RED + "Unable to execute this command.");
		} else {
			target = Bukkit.getServer().getPlayer(args[0]);

			if (target == null) {
				sender.sendMessage(ChatColor.RED + "Cannot find player '" + args[0] + "'");
				return true;
			}
		}

		if (commandLabel.equalsIgnoreCase("team")) {
			if (args.length == 1 && commandLabel.equalsIgnoreCase("accept")) {
				Player s = (Player) sender;

				if (target.equals(Vars.teamLeader.containsKey(Vars.teamID))) {

					for (Integer ID : Vars.teamID) {
						if (ID.equals(Vars.teamID.contains("\\d+"))) {
							s.sendMessage("You are already in a team.");
							return true;
						}
						Vars.teamID.equals(Vars.teamLeader);
					}
					sender.sendMessage(ChatColor.GREEN + "You have joined " + target.getName() + "'s team.");
					target.sendMessage(ChatColor.GREEN + sender.getName() +" has joined your team.");
				
					Vars.inviting.remove(target);
					Vars.invited.remove(sender);
				}
			}
		}
		return true;
	}
}