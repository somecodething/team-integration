package org.MC3.sg.teams.teamCommands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class team implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player target;
		
		if (args.length == 0 && !(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
			return true;
		}
		if (commandLabel.equalsIgnoreCase("team")) {
			target = (Player) sender;
			target.sendMessage("" + ChatColor.RED + ChatColor.BOLD + "======== " + ChatColor.RED + ChatColor.BOLD + "Team Commands" + ChatColor.DARK_RED + ChatColor.BOLD + " ========");
			target.sendMessage(ChatColor.YELLOW + "/team create - Creates a team");
			target.sendMessage(ChatColor.YELLOW + "/team invite [player] - Invites player to your team");
			target.sendMessage(ChatColor.YELLOW + "/team accept - Accepts an invite to a team");
			target.sendMessage(ChatColor.YELLOW + "/team deny - Denies an invite to a team");
			target.sendMessage(ChatColor.YELLOW + "/team kick [player] - Kicks a player from your party (Team leader only)");
			target.sendMessage(ChatColor.YELLOW + "/team leave - Leaves current team");
			target.sendMessage(ChatColor.YELLOW + "/team list [player]- List players in the team");
		}
		return true;
	}
}