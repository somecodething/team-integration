package org.MC3.sg.teams.teamCommands;

import org.MC3.sg.Vars;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class kick implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player target;
		
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
		}
		
		if (args.length == 0) {
			target = (Player) sender;
			sender.sendMessage(ChatColor.RED + "Just use /team leave " + sender.getName() + ".");
		} else {
			target = Bukkit.getServer().getPlayer(args[0]);
			
			if (target == null) {
				sender.sendMessage(ChatColor.RED + "Cannot find player '" + args[0] + "'");
			}
		}
		if (commandLabel.equalsIgnoreCase("team")) {
			if (args.length == 1 && args[0].equalsIgnoreCase("kick") && sender.equals(Vars.teamLeader.containsKey(Vars.teamID))) {
			
				sender.sendMessage(ChatColor.YELLOW + "You have kicked " + target.getName() + "from the team.");
				target.sendMessage(ChatColor.YELLOW + "You have been kicked from the team.");
				
				Vars.teamID.remove(target);
			}
		}
		return true;
	}

}