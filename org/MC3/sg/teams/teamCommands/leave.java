package org.MC3.sg.teams.teamCommands;

import org.MC3.sg.Vars;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class leave implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player team = (Player) Bukkit.getOnlinePlayers();
		
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
		}
		if (commandLabel.equalsIgnoreCase("team")) {
			if (args.length == 1 && args[0].equalsIgnoreCase("leave")) {
				if (sender.equals(Vars.teamLeader)) {
					Vars.teamLeader.remove(sender);
					Vars.teamID.remove(sender);
					if (Vars.teamID.equals(Vars.teamLeader.get(sender))) {
						Vars.teamID.remove("\\d+"); // Can this also be done? Vars.teamID.remove(team)
						team.sendMessage("Your team leader has left and the team has been disbanded.");
					}
					sender.sendMessage("You have left and disbanded your team.");
				}
				if (team.equals(Vars.teamID.equals(sender))) {
					team.sendMessage(sender.getName() + " has left the team.");
				}
				sender.sendMessage("You have left the team.");
				Vars.teamID.remove("\\d+");
			}
		}
		return true;
	}
}
